#!/usr/bin/env nodejs

var express     = require('express')
  , app         = express()
  , Promise     = require('bluebird')
  , localConf   = require('./local.json')
  , mongoose    = require('mongoose')
  , port        = localConf.apiServer.match('://.*:([0-9]+)')[1]
  , util        = require('util')
  , bodyParser  = require('body-parser')
  , url         = require('url')
  , TracerDebug = require('tracer-debug')
  , tracer      = new TracerDebug()
  ;


function makeResponse(code, message, data) {
  return {
    code: code,
    message: message,
    data: data
  }
};

function connectDB(options) {
  return new Promise(function(resolve, reject) {
    var uri = url.resolve(options.databaseUrl, options.database);
    tracer.log(util.format("Connecting to %s ...", uri));
    mongoose.connect(uri, function(err) {
      if (err) reject(err);
      else resolve();
    });
  });
};

function handleRequest(req, res, next) {
  var msg = util.format('Processing "%s" endpoint with %s method', req.path, req.method);
  tracer.info(msg);
  req.makeResponse = makeResponse;
  next();
};

function main() {
  var router  = express.Router()
    , index   = require('./routes/index')(mongoose)
    , users   = require('./routes/users')(mongoose)
    , locations = require('./routes/locations')(mongoose)
    , prefs   = require('./routes/prefs')(mongoose)
    ;

  var maxBodySize = '200mb';
  app.use(bodyParser.urlencoded({ extended: true, limit: maxBodySize }));
  app.use(bodyParser.json({ limit: maxBodySize }));

  var apiRoot = localConf.apiServer.match('://.*:[0-9]+(.*)')[1];
  app.use(handleRequest);
  app.use(apiRoot, router);
  app.use(apiRoot, index);
  app.use(apiRoot + '/users', users);
  app.use(apiRoot + '/locations', locations);
  app.use(apiRoot + '/prefs', prefs);

  app.listen(port, function() {
    console.log(util.format("Serving API %s on port %d", apiRoot, port));
  })
};


connectDB(localConf).then(main).catch(function(reason) {
  console.error(reason);
  process.exit(1);
});
