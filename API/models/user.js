module.exports = function(mongoose, database) {

  mongoose = mongoose || require('mongoose');
  var db = mongoose.connection.useDb(database || 'users');

  var User = new mongoose.Schema({
    username:     { type: String, required: true },
    password:     { type: String, required: true },
    rol:          { type: String, required: true },
    name:         { type: String, required: true },
    surname:      { type: String, required: true },
    sex:          { type: String, required: true },
    related:      Array,
  }, {
    timestamps: true,
    strict: false
  });

  User.index({ username: 1 }, { unique: true });

  var UserModel = db.model('User', User);

  return UserModel;
};
