module.exports = function(mongoose, database) {

  mongoose = mongoose || require('mongoose');
  var db = mongoose.connection.useDb(database || 'locations');

  var Location = new mongoose.Schema({
    username:     { type: String, required: true },
    latitude:     { type: String, required: true },
    longitude:    { type: String, required: true },
    street:       { type: String, required: true },
    province:     { type: String, required: true },
  }, {
    timestamps: true,
    strict: false
  });

  Location.index({ username: 1, createdAt: 1 }, { unique: true });

  var LocationModel = db.model('Location', Location);

  return LocationModel;
};
