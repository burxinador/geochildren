module.exports = function(mongoose, database) {

  mongoose = mongoose || require('mongoose');
  var db = mongoose.connection.useDb(database || 'prefs');

  var Pref = new mongoose.Schema({
    username:     { type: String, required: true },
    period:       { type: String, required: true },
  }, {
    timestamps: true,
    strict: false
  });

  Pref.index({ username: 1, period: 1 }, { unique: true });

  var PrefModel = db.model('Pref', Pref);

  return PrefModel;
};
