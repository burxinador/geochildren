#!/usr/bin/env nodejs

var Promise         = require('bluebird')
  , hash            = require('password-hash')
  , User
  , PrefModel
  ;


function updateMinutes(options) {
  return User.signIn(options)
  .then(function() {
    return PrefModel.findOneAndUpdate({ username: options.child }, { minutes: options.minutes }, { upsert: true, new: true }).exec()
  })
};

function getMinutes(options) {
  return User.signIn(options)
  .then(function() {
    return PrefModel.findOne({ username: options.username }).lean().exec()
  })
};


var Pref = function(mongoose, database) {
  PrefModel = require('../models/pref')(mongoose);
  User = require('./user')(mongoose);
  mongoose.Promise = Promise;

  return {
    updateMinutes: updateMinutes,
    getMinutes: getMinutes,
  }
}

module.exports = Pref;
