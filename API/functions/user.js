#!/usr/bin/env nodejs

var Promise     = require('bluebird')
  , hash        = require('password-hash')
  , UserModel
  ;


function signUp(options) {
  options.password = hash.generate(options.password);
  return UserModel(options).save();
};

function signIn(options) {
  return UserModel.findOne({ username: options.username }).lean().exec()
  .then(function(user) {
    if (!user) {
      return Promise.reject({ codeApi: 3, message: "User not found" });
    } else if (options.password != user.password && !hash.verify(options.password, user.password)) {
      return Promise.reject({ codeApi: 4, message: "Password do not match" });
    } else {
      return user;
    }
  })
};

function addRelated(options) {
  return signIn(options)
  .tap(function(user) {
    return UserModel.findOne({ username: options.relatedUser }).lean().exec()
    .then(function(userRelated) {
      if (!userRelated || userRelated.rol !== 'child')
        return Promise.reject({ codeApi: 3, message: "Child not found" })
      var id = userRelated._id;
      delete userRelated._id;
      if (userRelated.related.indexOf(options.username) === -1)
        userRelated.related.push(options.username);
      return UserModel.findByIdAndUpdate(id, { related: userRelated.related }).exec()
    })
  })
  .then(function(user) {
    if (user.related.indexOf(options.relatedUser) === -1)
      user.related.push(options.relatedUser);
    var id = user._id;
    return UserModel.findByIdAndUpdate(id, { related: user.related }, { new: true }).exec()
  })
  .then(function(user) {
    return composeRelated(user.related);
  })
};

function composeRelated(related) {
  return Promise.reduce(related, function(relatedUsers, username) {
    return UserModel.findOne({ username: username }).exec()
    .then(function(relatedUser) {
      if (relatedUser)
        relatedUsers[relatedUser.username] = { name: relatedUser.name, surname: relatedUser.surname, sex: relatedUser.sex };
      return relatedUsers;
    })
  }, { })
};

function retrieveRelated(options) {
  return signIn(options)
  .then(function(user) {
    return composeRelated(user.related);
  })
};

function deleteChild(options) {
  return signIn(options)
  .tap(function(user) {
    return UserModel.findOne({ username: options.child }).lean().exec()
    .then(function(userRelated) {
      if (!userRelated || userRelated.rol !== 'child')
        return Promise.reject({ codeApi: 3, message: "Child not found" })
      var index = userRelated.related.indexOf(options.username);
      if (index !== -1) {
        userRelated.related.splice(index, 1);
      }
      var id = userRelated._id;
      delete userRelated._id;
      return UserModel.findByIdAndUpdate(id, { related: userRelated.related }).exec();
    })
  })
  .then(function(user) {
    var index = user.related.indexOf(options.child);
    if (index != -1) {
      user.related.splice(index, 1);
    }
    var id = user._id;
    delete user._id;
    return UserModel.findByIdAndUpdate(id, { related: user.related }, { new: true }).exec();
  })
  .then(function(user) {
    return composeRelated(user.related);
  })
};

var User = function(mongoose, database) {
  UserModel = require('../models/user')(mongoose);
  mongoose.Promise = Promise;

  return {
    signUp: signUp,
    signIn: signIn,
    addRelated: addRelated,
    retrieveRelated: retrieveRelated,
    deleteChild: deleteChild,
  }
}

module.exports = User;
