#!/usr/bin/env nodejs

var Promise         = require('bluebird')
  , hash            = require('password-hash')
  , User
  , LocationModel
  ;


function addLocation(options) {
  return User.signIn(options)
  .then(function() {
    return LocationModel.findOne({}).sort('-createdAt').lean().exec()
  })
  .then(function(oldLocation) {
    if (! (oldLocation
      && oldLocation.street === options.street
      && oldLocation.province === options.province)) {
      delete options.password;
      return LocationModel(options).save();
    }
  })
};

function getQueryDate(query) {
  var currentDay = new Date();
  var days = 0;
  switch (query) {
    case 'ld': days = 1; break;
    case 'lw': days = 7; break;
    case 'lm': days = 30; break;
  }
  currentDay.setDate(currentDay.getDate()-days);
  return currentDay;
};

function getLocation(options) {
  return User.signIn(options)
  .then(function() {
    if (options.query === 'll') {
      return LocationModel.findOne({ username: options.child }).sort('-createdAt').exec()
    } else {
      return LocationModel.find({ 
        username: options.child,
        createdAt: { $gte: getQueryDate(options.query) }
      })
      .sort('-createdAt')
      .exec()
    }
  }) 
};


var Location = function(mongoose, database) {
  LocationModel = require('../models/location')(mongoose);
  User = require('./user')(mongoose);
  mongoose.Promise = Promise;

  return {
    addLocation: addLocation,
    getLocation: getLocation,
  }
}

module.exports = Location;
