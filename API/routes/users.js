var router = require('express').Router()
  , TracerDebug = require('tracer-debug')
  , tracer = new TracerDebug()
  , util = require('util')
  , User 
  ;


router.get('/signin/:username/:password', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  
  if (! (username && password)) {
    tracer.error("Missing param:", username, password);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    User.signIn({
      username: username,
      password: password
    })
    .then(function(user) {
      tracer.info(util.format('User %s signed in', username));
      res.send(req.makeResponse(0, "User signed in", user));
    })
    .catch(function(err) {
      var code = err.codeApi || 1;
      var message = err.message || "User not signed in";
      tracer.error(util.format("User %s not signed in, code %d", username, code));
      res.send(req.makeResponse(code, message, err));
    })
  }
});

router.post('/signup/:username/:password/:rol/:name/:surname/:sex', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  var rol = req.params.rol; 
  var name = req.params.name;
  var surname = req.params.surname;
  var sex = req.params.sex;

  if (! (username && password && rol && name && surname && sex)) {
    tracer.error("Missing param:", username, password, rol, name, surname, sex);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    User.signUp({
      username: username,
      password: password,
      rol: rol,
      name: name,
      surname: surname,
      sex: sex
    })
    .then(function(user) {
      tracer.info(util.format("User %s signed up.", user.username));
      res.send(req.makeResponse(0, "User signed up", user));
    })
    .catch(function(err) {
      tracer.error(err);
      var code = err.code === 11000 ? 5 : 1;
      res.send(req.makeResponse(code, "User not signed up", err));
    })
  }
});

router.get('/related/:username/:password', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;

  if (! (username && password)) {
    tracer.info("Missing param:", username, password);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    User.retrieveRelated({ username: username, password: password })
    .then(function(related) {
      if (Object.keys(related).length)
        res.send(req.makeResponse(0, "Related users", related));
      else
        res.send(req.makeResponse(6, "Empty related users", null));
    })
    .catch(function(err) {
      var code = err.codeApi || 1;
      var message = err.message || "Related users not retrieved";
      tracer.error(util.format("Related users for %s not retrieved, code %d, err %j", username, code, err));
      res.send(req.makeResponse(code, message, null));
    })
  }
});

router.put('/related/delete/:username/:password/:child', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  var child = req.params.child;

  if (! (username && password && child)) {
    tracer.info("Missing param:", username, password);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    User.deleteChild({ username: username, password: password, child: child })
    .then(function(userModified) {
      res.send(req.makeResponse(0, "Child deleted", userModified));
    })
    .catch(function(err) {
      var code = err.codeApi || 1;
      var message = err.message || "Child not deleted";
      tracer.error(util.format("Child %s for %s not deleted, code %d, err %j", child, username, code, err));
      res.send(req.makeResponse(code, message, null));
    })
  }
});

router.put('/related/add/:username/:password/:child', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  var newUser = req.params.child;
  
  if (! (username && password && newUser)) {
    tracer.info("Missing param:", username, password);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    User.addRelated({ username: username, password: password, relatedUser: newUser })
    .then(function(userModified) {
      res.send(req.makeResponse(0, "Child added to related", userModified));
    })
    .catch(function(err) {
      var code = err.codeApi || 1;
      var message = err.message || "Child not added";
      tracer.error(util.format("Child %s for %s not added, code %d, err %j", newUser, username, code, err));
      res.send(req.makeResponse(code, message, null));
    })
  }
});


module.exports = function(mongoose, conf) {
  User = require('../functions/user')(mongoose);
  return router;
}
