var router = require('express').Router()
  , TracerDebug = require('tracer-debug')
  , tracer = new TracerDebug()
  , util = require('util')
  , Location 
  ;


router.post('/:username/:password/:latitude/:longitude/:street/:province', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  var latitude = req.params.latitude;
  var longitude = req.params.longitude;
  var street = req.params.street;
  var province = req.params.province;

  if (! (username && password && latitude && longitude && street && province)) {
    tracer.error("Missing param:", username, password, latitude, longitude, street, province);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    Location.addLocation({
      username: username,
      password: password,
      latitude: latitude,
      longitude: longitude,
      street: street,
      province: province
    })
    .then(function(newLoc) {
      var message = newLoc ? "New location saved for user" : "Location repeated for user";
      tracer.info(message, username);
      res.send(req.makeResponse(0, message, newLoc || null));
    })
    .catch(function(err) {
      tracer.error(err);
      var code = err.codeApi || 1;
      var message = err.message || "Could not save the location";
      res.send(req.makeResponse(code, message, null));
    })
  }
});

router.get('/:username/:password/:child?', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  var child = req.params.child;
  var query = req.query.d;

  if (! (username && password && child && query)) {
    tracer.error("Missing param:", username, password, child, query);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    Location.getLocation({
      username: username,
      password: password,
      child: child,
      query: query
    })
    .then(function(locations) {
      res.send(req.makeResponse(0, "Locations retrieved", locations));
    })
    .catch(function(err) {
      tracer.error(err);
      var code = err.codeApi || 1;
      var message = err.message || "Could not retrieve the locations";
      res.send(req.makeResponse(code, message, null));
    })
  }
});


module.exports = function(mongoose, conf) {
  Location = require('../functions/location')(mongoose);
  return router;
}
