var router = require('express').Router()
  , TracerDebug = require('tracer-debug')
  , tracer = new TracerDebug()
  , util = require('util')
  , Pref 
  ;


router.put('/:username/:password/:minutes/:child', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  var minutes = req.params.minutes;
  var child = req.params.child;
  if (! (username && password && minutes)) {
    tracer.error("Missing param:", username, password, minutes);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    Pref.updateMinutes({
      username: username,
      password: password,
      minutes: minutes,
      child: child
    })
    .then(function(pref) {
      if (!pref) {
        res.send(req.makeResponse(6, "Minutes not fixed", null));
      } else {
        res.send(req.makeResponse(0, "Minutes updated", pref.minutes));
      }
    })
    .catch(function(err) {
      var code = err.codeApi || 1;
      var message = err.message || "Minutes not updated";
      tracer.error(util.format("Minutes for child %s not updated", child));
      res.send(req.makeResponse(code, message, err));
    })
  }
});

router.get('/:username/:password', function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  if (! (username && password)) {
    tracer.error("Missing param:", username, password);
    res.send(req.makeResponse(2, "Missing param", null));
  } else {
    Pref.getMinutes({
      username: username,
      password: password
    })
    .then(function(pref) {
      if (!pref) {
        res.send(req.makeResponse(6, "Minutes not fixed yet", null));
      } else {
        res.send(req.makeResponse(0, "Minutes retrieved", pref.minutes));
      }
    })
    .catch(function(err) {
      var code = err.codeApi || 1;
      var message = err.message || "Minutes not retrieved";
      tracer.error(util.format("Minutes for child %s not retrieved", username));
      res.send(req.makeResponse(code, message, err));
    })
  }
});


module.exports = function(mongoose, conf) {
  Pref = require('../functions/pref')(mongoose);
  return router;
}
