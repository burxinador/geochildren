var router = require('express').Router()
  , TracerDebug = require('tracer-debug')
  , tracer = new TracerDebug()
  ;

router.get('/', function(req, res) {
  res.send("Welcome to GeoChildren's API!").end();
});

module.exports = function(mongoose, conf) {
  return router;
};
