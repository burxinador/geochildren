package com.example.carlos.geochildren.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carlos.geochildren.R;

/**
 * Created by efrencamarasacarreres on 16/4/16.
 */
public class MenuChildrenActivity extends AppCompatActivity {

    String name;
    String username;
    String sex;
    TextView tvName;
    TextView tvUsername;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.children_rectangle_with_menu);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        name = prefs.getString("active_child_name","No Name");
        username = prefs.getString("active_child_username","No Username");
        sex = prefs.getString("active_child_sex","No Sex");

        tvName = (TextView) findViewById(R.id.children_name_rectangle);
        tvUsername = (TextView) findViewById(R.id.children_username_rectangle);
        image = (ImageView) findViewById(R.id.image_menu_children);

        tvName.setText(name);
        tvUsername.setText(username);
        if(sex.compareTo("m") == 0){
            image.setImageResource(R.drawable.son);
        }
        else{
            image.setImageResource(R.drawable.daughter);
        }
    }

    public void buttons_functions(View v){
        Intent intent = null;
        //Según el botón, hacemos una acción u otra
        switch (v.getId()){
            case R.id.geo_pos_menu:
                intent = new Intent(this,ChildMapActivity.class);
                break;

            case R.id.geo_his_menu:
                intent = new Intent(this,ChildHistoryMapActivity.class);
                break;
            case R.id.geo_admin_apps_menu:
                break;

            case R.id.geo_admin_webs_menu:
                break;

            case R.id.geo_chat_menu:
                break;

            case R.id.geo_preferences_menu:
                intent = new Intent(MenuChildrenActivity.this,PreferencesChildrenActivity.class);
                break;
        }
        if(intent != null){
            startActivity(intent);
        }
    }
}
