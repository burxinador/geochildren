package com.example.carlos.geochildren.Pojo;

/**
 * Created by efrencamarasacarreres on 18/4/16.
 */
public class Position {

    String longitud;
    String latitud;
    String street;
    String city_country;
    String date;

    public Position() {
        longitud = "No Longitud";
        latitud = "No Latitud";
        street = "No Street";
        city_country = "No City";
        date = "No Date";
    }

    public Position(String longitud, String latitud, String street, String city_country, String date) {
        this.longitud = longitud;
        this.latitud = latitud;
        this.street = street;
        this.city_country = city_country;
        this.date = date;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity_country() {
        return city_country;
    }

    public void setCity_country(String city_country) {
        this.city_country = city_country;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
