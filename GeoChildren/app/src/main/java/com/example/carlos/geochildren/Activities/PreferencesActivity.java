package com.example.carlos.geochildren.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.Toast;

import com.example.carlos.geochildren.R;

public class PreferencesActivity extends AppCompatActivity {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Switch not;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        //Recuperamos el estado del switch
        not = (Switch) findViewById(R.id.notifications);
        not.setChecked(prefs.getBoolean("notifications",true));

        Boolean isParent = prefs.getBoolean("isParent",true);
        TableRow control_zone;
        //Si es un parent aparece la opción de contol zone en preferencias, si es un niño no
        if(!isParent){
            control_zone = (TableRow) findViewById(R.id.control_zones);
            control_zone.setVisibility(View.INVISIBLE);
        }
        else{
            control_zone = (TableRow) findViewById(R.id.control_zones);
            control_zone.setVisibility(View.VISIBLE);
        }
    }

    public void buttons_functions(View v){
        //Según el botón pulsado hacemos una acción u otra
        switch (v.getId()){
            case R.id.control_zones:

                break;

            case R.id.change_image:

                break;

            //Los siguientes botones muestran un diálogo para la inserción del valor para el cambio
            case R.id.change_name:
                final AlertDialog.Builder change_name = new AlertDialog.Builder(PreferencesActivity.this);
                change_name.setTitle(R.string.change_name_title);
                change_name.setMessage(R.string.change_name_message);
                EditText username = new EditText(PreferencesActivity.this);
                username.setHint(R.string.hint_change_name);
                change_name.setView(username);

                change_name.setPositiveButton(R.string.accept_change_name, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PreferencesActivity.this, R.string.petition_child, Toast.LENGTH_SHORT).show();
                    }
                });
                change_name.setNegativeButton(R.string.cancel_change_name, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                change_name.create().show();
                break;

            case R.id.change_first_surname:
                final AlertDialog.Builder change_first_surname = new AlertDialog.Builder(PreferencesActivity.this);
                change_first_surname.setTitle(R.string.change_surnames_title);
                change_first_surname.setMessage(R.string.change_surnames_message);
                EditText surname = new EditText(PreferencesActivity.this);
                surname.setHint(R.string.hint_surnames_surname);
                change_first_surname.setView(surname);

                change_first_surname.setPositiveButton(R.string.accept_change_first_surname, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PreferencesActivity.this, R.string.petition_child, Toast.LENGTH_SHORT).show();
                    }
                });
                change_first_surname.setNegativeButton(R.string.cancel_change_first_surname, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                change_first_surname.create().show();
                break;

            case R.id.change_email:
                final AlertDialog.Builder change_email = new AlertDialog.Builder(PreferencesActivity.this);
                change_email.setTitle(R.string.change_email_title);
                change_email.setMessage(R.string.change_email_message);
                EditText email = new EditText(PreferencesActivity.this);
                email.setHint(R.string.hint_change_email);
                change_email.setView(email);

                change_email.setPositiveButton(R.string.accept_change_email, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PreferencesActivity.this, R.string.petition_child, Toast.LENGTH_SHORT).show();
                    }
                });
                change_email.setNegativeButton(R.string.cancel_change_email, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                change_email.create().show();
                break;

        }
    }

    @Override
    protected void onPause() {
        //Guargar el valor del switch
        not = (Switch) findViewById(R.id.notifications);
        editor.putBoolean("notifications",not.isChecked());
        editor.apply();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,MainActivity.class));
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this,MainActivity.class));
        return super.onOptionsItemSelected(item);
    }
}
