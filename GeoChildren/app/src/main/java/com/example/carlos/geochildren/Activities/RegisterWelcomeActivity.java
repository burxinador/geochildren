package com.example.carlos.geochildren.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.carlos.geochildren.Pojo.User;
import com.example.carlos.geochildren.R;
import com.example.carlos.geochildren.Tasks.PostUserAsyncTask;
import com.example.carlos.geochildren.Tasks.PostUserWelcomeAsyncTask;

import java.util.regex.Pattern;

public class RegisterWelcomeActivity extends AppCompatActivity {

    boolean get_again = false;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    PostUserWelcomeAsyncTask task;
    ConnectivityManager manager;
    NetworkInfo info;
    EditText username, name, both_surnames, password, password_conf, email;
    RadioGroup groupSex, groupType;
    String sex, type, username_string, name_string, both_surnames_string, password_string, password_conf_string, email_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_welcome);

        username = (EditText) findViewById(R.id.etusername_welcome);
        name = (EditText) findViewById(R.id.etnameuser_welcome);
        both_surnames = (EditText) findViewById(R.id.etbothsurnames_welcome);
        password = (EditText) findViewById(R.id.etpassword_welcome);
        password_conf = (EditText) findViewById(R.id.etpasswordcomp_welcome);
        email = (EditText) findViewById(R.id.etmail_welcome);
        groupSex = (RadioGroup) findViewById(R.id.groupSex_welcome);
        groupType = (RadioGroup) findViewById(R.id.groupType_welcome);
    }

    public boolean samePasswords() {
        return (password_string.compareTo(password_conf_string) == 0);
    }

    public boolean noneEmpty() {
        if ((username_string.compareTo("")) == 0 || (username_string == null)) {
            username.requestFocus();
            return false;
        } else {
            if ((name_string.compareTo("") == 0) || (name_string == null)) {
                name.requestFocus();
                return false;
            } else {
                if ((both_surnames_string.compareTo("") == 0) || (both_surnames_string == null)) {
                    both_surnames.requestFocus();
                    return false;
                } else {
                    if ((password_string.compareTo("") == 0) || (password_string == null)) {
                        password.requestFocus();
                        return false;
                    } else {
                        if ((password_conf_string.compareTo("") == 0) || (password_conf_string == null)) {
                            password_conf.requestFocus();
                            return false;
                        } else {
                            if ((email_string.compareTo("") == 0) || (email_string == null)) {
                                email.requestFocus();
                                return false;
                            } else {
                                if ((groupSex.getCheckedRadioButtonId() == -1)) {
                                    groupSex.requestFocus();
                                    return false;
                                } else {
                                    if (groupType.getCheckedRadioButtonId() == -1) {
                                        groupType.requestFocus();
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    //Solo un botón puede llamar a este método
    public void button_function(View v) {

        username_string = username.getText().toString();
        name_string = name.getText().toString();
        both_surnames_string = both_surnames.getText().toString();
        password_string = password.getText().toString();
        password_conf_string = password_conf.getText().toString();
        email_string = email.getText().toString();

        Pattern spaces = Pattern.compile("\\s+");

        username_string = username_string.trim();
        name_string = spaces.matcher(name_string.trim()).replaceAll("%20");
        both_surnames_string = spaces.matcher(both_surnames_string.trim()).replaceAll("%20");
        password_string = spaces.matcher(password_string.trim()).replaceAll("%20");
        password_conf_string = spaces.matcher(password_conf_string.trim()).replaceAll("%20");
        email_string = spaces.matcher(email_string.trim()).replaceAll("%20");

        if (groupSex.getCheckedRadioButtonId() == R.id.rbMale_welcome) {
            sex = "m";
        } else {
            sex = "f";
        }

        if (groupType.getCheckedRadioButtonId() == R.id.rbParent_welcome) {
            type = "parent";
        } else {
            type = "child";
        }

        if (noneEmpty()) {
            if (samePasswords()) {
                //Registar en el servidor
                manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                info = manager.getActiveNetworkInfo();
                if ((info != null) && info.isConnected()) {
                    //pbRegister.setVisibility(ProgressBar.VISIBLE);
                    task = new PostUserWelcomeAsyncTask();
                    task.setParent(RegisterWelcomeActivity.this);
                    String[] params = {username_string, password_string, type, name_string, both_surnames_string, sex};
                    task.execute(params);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterWelcomeActivity.this);
                    builder.setTitle(R.string.connection_off_title);
                    builder.setMessage(R.string.connection_off_message);
                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            get_again = true;
                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    builder.create().show();
                }
            } else {
                password.requestFocus();
                Toast.makeText(this, R.string.password_not_match, Toast.LENGTH_SHORT);
            }
        } else {
            Toast.makeText(this, R.string.no_all_paramenters, Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onResume() {
        if (get_again) {
            manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            info = manager.getActiveNetworkInfo();
            if ((info != null) && info.isConnected()) {
                //pbScores.setVisibility(ProgressBar.VISIBLE);
                task = new PostUserWelcomeAsyncTask();
                task.setParent(RegisterWelcomeActivity.this);
                String[] params = {username_string, password_string, type, name_string, both_surnames_string, sex};
                get_again = false;
                task.execute(params);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterWelcomeActivity.this);
                builder.setTitle(R.string.connection_off_title);
                builder.setMessage(R.string.connection_off_message);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get_again = true;
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                });
                builder.create().show();
            }
        }
        super.onResume();
    }

    public void loginUser (User user){
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();
        editor.putString("active_user_username",user.getUsername());
        editor.putString("active_user_name",user.getName());
        editor.putString("active_user_sex",user.getSex());
        editor.putString("active_user_password",user.getPassword());
        if(user.getRol().compareTo("parent") == 0) {
            editor.putBoolean("isParent",true);
        }
        else{
            editor.putBoolean("isParent",false);
        }
        editor.putBoolean("logged",true);
        editor.putBoolean("first_time",false);
        editor.apply();
        this.finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,WelcomeActivity.class));
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this,WelcomeActivity.class));
        return super.onOptionsItemSelected(item);
    }

    public void sameUsername(){
        Toast.makeText(this, R.string.user_exist,Toast.LENGTH_SHORT).show();
    }
}
