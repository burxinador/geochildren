package com.example.carlos.geochildren.Tasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.carlos.geochildren.Activities.RegisterActivity;
import com.example.carlos.geochildren.Activities.RegisterWelcomeActivity;
import com.example.carlos.geochildren.Pojo.User;
import com.example.carlos.geochildren.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by efrencamarasacarreres on 18/4/16.
 */
    public class PostUserWelcomeAsyncTask extends AsyncTask<String,Void,User> {
    //Para insertar un usuario

    RegisterWelcomeActivity parent;
    String username;
    String password;
    String rol;
    String name;
    String surname;
    String sex;
    User user;

    public static String convertStreamToString(java.io.InputStream is){
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next():"";
    }

    public void setParent(RegisterWelcomeActivity parent){
        this.parent = parent;
    }

    @Override
    protected User doInBackground(String... params) {
        username = params[0];
        password = params[1];
        rol = params[2];
        name = params[3];
        surname = params[4];
        sex = params[5];

        InputStreamReader isr = null;
        HttpURLConnection connection = null;
        /*
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http");
        builder.authority("213.201.97.154:8081");
        builder.appendPath("ps");
        builder.appendPath("v1");
        builder.appendPath("users");
        builder.appendPath("signup");
        builder.appendPath(username);
        builder.appendPath(password);
        builder.appendPath(rol);
        builder.appendPath(name);
        builder.appendPath(surname);
        builder.appendPath(sex);
        */

        try {
            //URL url = new URL(builder.build().toString());
            URL url = new URL("http://213.201.97.154:8081/gc/v1/users/signup/"+username+"/"+password+"/"+rol+"/"+name+"/"+surname+"/"+sex);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                isr = new InputStreamReader(connection.getInputStream(), "UTF-8");

                String response = convertStreamToString(connection.getInputStream());
                JSONObject object = null;
                Integer code = -1;
                String message = null;
                try {
                    object = new JSONObject(response);
                    JSONObject data = object.getJSONObject("data");
                    code = object.getInt("code");
                    message = object.getString("message");

                    if(code == 0) {
                        //Gson gson = new Gson();
                        //user = gson.fromJson(isr, User.class);
                        //data.toString();
                        user = new User(data.getString("username"),data.getString("name"),data.getString("password"),
                                data.getString("surname"),data.getString("rol"),data.getString("sex"));
                    }
                    else{ //Code 5
                        user = null;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connection.disconnect();
        }
        return user;
    }

    @Override
    protected void onPostExecute(User user) {
        if(user != null){
            parent.loginUser(user);
        }
        else{
            parent.sameUsername();
        }
        super.onPostExecute(user);
    }
}

