# API

## Resposta
```json
{
  "code": 0 (tot bé) | 1 (error desconegut) | 2 (falta algun paràmetre),
  "message": "el missatge informant del resultat",
  "data": Object
}
```

## Instruccions d'ús (Linux)
```bash
  GET -> ~$ curl -X GET http://213.201.97.154:8081/gc/v1/<endpoint>
  POST -> ~$ curl -X POST http://213.201.97.154:8081/gc/v1/<endpoint>

  # Exemples: 
    curl -X GET http://213.201.97.154:8081/gc/v1/users/related/carlos/123456
    curl -X POST http://213.201.97.154:8081/gc/v1/users/signup/pepe/1234/parent/Pepe/Díaz%20Aguilar/m
```

## Rutes

### `Registrar un usuari`
```
  (POST) http://213.201.97.154:8081/gc/v1/users/signup/<username>/<password>/<rol>/<name>/<surname>/<sex>

  <username>: nick d'usuari
  <password>: contrasenya
  <rol>: si és parent o child
  <name>: nom real
  <surname>: cognoms separats per %20
  <sex>: m (home) o f (dona)

  Si code == 0: Torna un objecte amb les dades de l'usuari
  Si code == 5: Eixe username ja existeix
```

### `Autenticar un usuari`
```
  (GET) http://213.201.97.154:8081/gc/v1/users/signin/<username>/<password>

  <username>: nick d'usuari
  <password>: contrasenya

  Si code == 0: Torna un objecte amb les dades de l'usuari
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
```

### `Recuperar els related d'un usuari`
```
  (GET) http://213.201.97.154:8081/gc/v1/users/related/<username>/<password>

  <username>: nick d'usuari
  <password>: contrasenya

  Si code == 0: Torna un objecte indexat pels usernames i amb valors un objecte format per name, surname i sex
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
  Si code == 6: No té cap username com a related
```

### `Borrar un fill dels related d'un usuari`
```
  (PUT) http://213.201.97.154:8081/gc/v1/users/related/delete/<username>/<password>/<child>

  <username>: nick d'usuari
  <password>: contrasenya
  <child>: username del fill

  Si code == 0: Torna un objecte indexat pels usernames i amb valors un objecte format per name, surname i sex
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
  Si code == 6: No té cap username com a related
```

### `Afegir un fill als related d'un usuari`
```
  (PUT) http://213.201.97.154:8081/gc/v1/users/related/add/<username>/<password>/<child>

  <username>: nick d'usuari
  <password>: contrasenya
  <child>: username del fill

  Si code == 0: Torna un objecte amb totes les dades de l'usuari actualitzat
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
```

### `Afegir una nova localització`
```
  (POST) http://213.201.97.154:8081/gc/v1/locations/<username>/<password>/<latitude>/<longitude>/<street>/<province>

  <username>: nick d'usuari
  <password>: contrasenya
  <latitude>: latitud
  <longitude>: longitud
  <street>: carrer
  <province>: provincia

  Si code == 0: Torna un objecte amb la localització
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
```

### `Obtindre les localitzacions`
```
  (GET) http://213.201.97.154:8081/gc/v1/locations/<username>/<password>/<child>?d

  <username>: nick d'usuari
  <password>: contrasenya
  <child>: username del fill
  ?d: query per a la data:
    - "ll": indica last location
    - "ld": indica last day
    - "lw": indica last week
    - "lm": indica last month
    · exemple: ?d="ld"

  Si code == 0: Torna un array de objectes localitzacions menys en e cas de "ll", que torna un objecte localització.
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
```

### `Canviar la freqüència d'actualització de locations`
```
  (PUT) http://213.201.97.154:8081/gc/v1/prefs/<username>/<password>/<minutes>/<child>

  <username>: nick d'usuari
  <password>: contrasenya
  <minutes>: minuts de freqüència d'actualització de la localització
  <child>: username del fill

  Si code == 0: Torna els minuts que s'acaben d'actualitzar
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
```

### `Obtindre la freqüència d'actualizació de locations`
```
  (GET) http://213.201.97.154:8081/gc/v1/prefs/<username>/<password>

  <username>: nick d'usuari del child
  <password>: contrasenya

  Si code == 0: Torna els minuts d'actualització
  Si code == 3: Eixe username no existeix
  Si code == 4: La contrasenya no és correcta
  Si code == 6: No s'ha fixat encara el temps d'actualització
```
